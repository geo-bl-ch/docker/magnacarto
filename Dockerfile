FROM golang:alpine AS builder

ENV CGO_ENABLED=0 \
    GOOS=linux

RUN apk --update add bash git make && \
    git clone https://github.com/omniscale/magnacarto.git && \
    cd magnacarto && \
    git checkout v1.1.0 && \
    make install && \
    find . -name 'app' && \
    find / -name 'magnacarto' && \
    find / -name 'magnaserv'

FROM registry.gitlab.com/geo-bl-ch/docker/mapnik:v3.0.23

USER 0

ENV MS_VERSION=7.4.4 \
    LD_LIBRARY_PATH="/opt/lib:/usr/local/lib"

# Install mapserver
RUN apk --update add \
        curl \
        protobuf-c \
        libpng \
        libjpeg \
        freetype \
        fribidi \
        harfbuzz \
        librsvg \
        fcgi \
        libxml2 \
        libpq && \
    apk --update add --virtual .mapserver-deps \
        make \
        cmake \
        gcc \
        g++ \
        curl-dev \
        protobuf-c-dev \
        jpeg-dev \
        libpng-dev \
        freetype-dev \
        fribidi-dev \
        harfbuzz-dev \
        librsvg-dev \
        fcgi-dev \
        postgresql-dev \
        libxml2-dev && \
    cd /tmp && \
    wget http://download.osgeo.org/mapserver/mapserver-${MS_VERSION}.tar.gz && \
    tar -zxf mapserver-${MS_VERSION}.tar.gz && \
    rm -f mapserver-${MS_VERSION}.tar.gz && \
    cd mapserver-${MS_VERSION} && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DCMAKE_PREFIX_PATH=/usr/local:/opt \
        -DWITH_CLIENT_WFS=1 \
        -DWITH_CLIENT_WMS=1 \
        -DWITH_CURL=1 \
        -DWITH_GIF=0 \
        -DWITH_RSVG=1 \
        -DWITH_SVGCAIRO=0 \
        ../ >../configure.out.txt && \
    make && \
    make install && \
    cd ~ && \
    apk del .mapserver-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

WORKDIR /run

ENV PATH="${PATH}:/run" \
    PROJECT_DIR=/projects \
    SHAPEFILE_DIR=/data \
    SQLITE_DIR=/data \
    IMAGE_DIR=/data

COPY --chown=1001:0 --from=builder /go/magnacarto/app /run/app
COPY --chown=1001:0 --from=builder /go/bin/magnacarto /run/
COPY --chown=1001:0 --from=builder /go/bin/magnaserv /run/
COPY --chown=1001:0 config.tml /config/config.tml
COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

RUN mkdir -p /projects && \
    mkdir -p /data && \
    apk --update add bash gettext && \
    adduser -h /run -u 1001 -G root -s /bin/bash -D magna && \
    chown -R 1001:0 /run /config /projects /data && \
    chmod -R g=u /run /config /projects /data

USER 1001

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["magnaserv", "-builder", "mapserver", "-listen", "0.0.0.0:8080", "-config", "config.tml"]
